﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        public SetPartnerPromoCodeLimitRequest CreateLimitRequest() => new SetPartnerPromoCodeLimitRequest
        {
            EndDate = DateTime.Now.AddDays(1),
            Limit = 5,
        };

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;
            SetPartnerPromoCodeLimitRequest limits = CreateLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limits);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            partner.IsActive = false;
            var limit = CreateLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limit);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SuccessWithActiveLimit_NullifiPrompcodesCount()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            partner.NumberIssuedPromoCodes = 100;
            var limit = CreateLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);


            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limit);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SuccessAndInactiveLimit_NotNullifiPrompcodesCount()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            partner.PartnerLimits.First().CancelDate = DateTime.UtcNow;
            partner.NumberIssuedPromoCodes = 100;
            var limit = CreateLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);


            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limit);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.NumberIssuedPromoCodes.Should().Be(100);

        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_Success_ShouldDisablePreviosLimit()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            var limit = CreateLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);


            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limit);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.PartnerLimits.First().CancelDate.Should().NotBeNull();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsZero_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            var limit = CreateLimitRequest();
            limit.Limit = 0;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);


            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limit);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_Success_ShouldAddNewLimitToDataBase()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            var limit = CreateLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);


            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limit);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            _partnersRepositoryMock.Invocations.Last().Method.Name.Should().Be(nameof(IRepository<Partner>.UpdateAsync));
            _partnersRepositoryMock.Invocations.Last().Arguments[0].Should().Be(partner);
            partner.PartnerLimits.Should().HaveCount(2);
            partner.PartnerLimits.Last().Limit.Should().Be(limit.Limit);
            partner.PartnerLimits.Last().EndDate.Should().Be(limit.EndDate);
        }
    }
}